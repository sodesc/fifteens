import pygame
import random


SIZE = WIDTH, HEIGHT = 810, 500
screen = pygame.display.set_mode(SIZE)


def load_image(name, colorkey=None):

    try:
        fullname = "data/" + name
        image = pygame.image.load(fullname)
    except pygame.error as message:
        print("Cannot load image:", name)
        raise SystemExit(message)
    image = image.convert_alpha()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey)
    return image


class Board:
    
    def __init__(self, group, image):

        self._board = []
        self.gather_whether()
        for i in self._arrange[:-1]:
            index = self._arrange.index(i)
            left = (i - 1) % 4 * 100
            top = (i - 1) // 4 * 100
            coords = (index % 4) * 100 + 10, (index // 4) * 100 + 10
            self._board += [(i, Puzzle(group, image, left, top, coords))]
        self._board += [(0, None)]

    def render(self):

        for y in range(4):
            for x in range(4):
                pygame.draw.rect(screen, (255, 255, 255),
                                 (100 * x + 10, 100 * y + 10,
                                  100, 100), 1)

    def get_cell(self, cords):

        x = cords[0] - 10
        y = cords[1] - 10
        return x // 100, y // 100

    def get_empty(self):

        for i in range(len(self._board)):
            if self._board[i][0] == 0:
                return i

    def update(self, cords):

        x = cords[0]
        y = cords[1]
        j = y * 4 + x
        z = self.get_empty()
        a = []
        for i in range(16):
            if i == j:
                a.append(self._board[z])
            elif i == z:
                a.append(self._board[j])
            else:
                a.append(self._board[i])
        self._board = a[:]

    def gather_whether(self):

        a = [i for i in range(1, 16)]
        random.shuffle(a)
        a.append(0)
        inv = 0
        for i in range(16):
            for j in range(i + 1, 16):
                if a[j] and a[i]:
                    if a[j] < a[i]:
                        inv += 1
        inv += a.index(0) // 4 + 1
        if inv % 2:
            a[5], a[6] = a[6], a[5]
        self._arrange = a[:]

    def check_win(self):

        global moves
        if (self._board[:-1] == sorted(self._board[:-1])
            and not self._board[-1][0]
            and moves > 0):
            return True


class Puzzle(pygame.sprite.Sprite):
    
    def __init__(self, group, image, left, top, coords):

        super().__init__(group)
        self.image = pygame.Surface((100, 100))
        self.image.blit(image, (0, 0), (left, top, 100, 100))
        self.rect = self.image.get_rect()
        self.rect.x = coords[0]
        self.rect.y = coords[1]
        self.empty_x = 310
        self.empty_y = 310

    def is_neighbour(self):

        x, y = (self.rect.x - 10) // 100, (self.rect.y - 10) // 100
        e1, e2 = (self.empty_x - 10) // 100, (self.empty_y - 10) // 100
        checks = ((x + 1, y),
                  (x - 1, y),
                  (x, y + 1),
                  (x, y - 1))
        for i, j in checks:
            if i == e1 and j == e2:
                return True
        return False

    def on_click(self, pos):

        x = pos[0]
        y = pos[1]
        if (self.rect.x <= x <= self.rect.x + 100 and \
            self.rect.y <= y <= self.rect.y + 100):
            return True
        return False

    def empty_upd(self):

        global empty_x, empty_y
        self.empty_x = empty_x
        self.empty_y = empty_y

    def update(self):

        self.rect.x = self.empty_x
        self.rect.y = self.empty_y


class Text(pygame.sprite.Sprite):
    
    def __init__(self, text, size, color, style):

        super(Text, self).__init__()
        self.color = pygame.Color(color)
        self.font = pygame.font.Font(style, size)
        self.set(text)

    def set(self, text):

        self.image = self.font.render(str(text), 1, self.color)
        self.rect = self.image.get_rect()


def time_format(time):

    time /= 1000
    minuts = int(time // 60)
    seconds = round(time % 60, 1)
    format_time = "Time     " + str(minuts) + ":" + str(seconds).rjust(4, "0")
    return format_time


def render_win():

    pygame.draw.rect(screen, (255, 255, 255), (465, 155, 260, 210), 5)
    screen.blit(win_image, (470, 160))


pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.init()
GALLERY = ["classic.png",
           "red.png",
           "rainbow.png"]
pygame.mixer.music.load("back.mp3")
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(0.4)
sound_of_move = pygame.mixer.Sound("move.ogg")
sound_of_win = pygame.mixer.Sound("win.ogg")
current_record = "       ___"
record_lable = [Text("Current", 30, "white", "GOUDYSTOUT.TTF"),
                Text("  record", 30, "white", "GOUDYSTOUT.TTF"),
                Text(current_record, 30, "cyan", "GOUDYSTOUT.TTF")]
win_image = load_image("win.png")
tried = False
running = True

while running:
    if not tried:
        empty_x = 310
        empty_y = 310
        image = load_image(random.choice(GALLERY))
        all_sprites = pygame.sprite.Group()
        board = Board(all_sprites, image)
        moves = 0
        time_out = Text("Time     0:0", 30, "white", "GOUDYSTOUT.TTF")
        moves_out = Text("Moves   0", 30, "white", "GOUDYSTOUT.TTF")
        start_button = Text("SHUFFLE", 50, "red", "AGENCY.TTF")
        button_color = pygame.Color("black")
        win_win = Text("SHUFFLE", 50, "red", "GOUDYSTOUT.TTF")
        keep_playing = True
        tried = True
    for event in pygame.event.get():
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            x, y = event.pos
            if 120 <= x <= 305 and \
               420 <= y <= 490:
                tried = False
                break
            for i in all_sprites:
                if (i.on_click(event.pos)
                    and i.is_neighbour()
                    and (keep_playing or not moves)):
                    sound_of_move.play()
                    if moves == 0:
                        time = pygame.time.get_ticks()
                        keep_playing = True
                    moves += 1
                    moves_out.set("Moves   " + str(moves))
                    board.update(board.get_cell(event.pos))
                    empty_x = i.rect.x
                    empty_y = i.rect.y
                    i.update()
                    break
            for j in all_sprites:
                j.empty_upd()
            if board.check_win():
                keep_playing = False
                if current_record == "       ___" or current_record > timer:
                    record_lable[2].set("     " + time_format(timer).split()[-1])
                    current_record = timer
                sound_of_win.play()
            if 120 <= x <= 305 and \
               420 <= y <= 490:
                button_color = pygame.Color("blue")
        if 120 <= mouse_x <= 305 and \
            420 <= mouse_y <= 490:
            button_color = pygame.Color("green")
        else:
            button_color = pygame.Color("black")
    if moves and keep_playing:
        timer = pygame.time.get_ticks() - time
        time_out.set(time_format(timer))
    screen.fill((0, 0, 0))
    board.render()
    all_sprites.draw(screen)
    screen.blit(time_out.image, (440, 400))
    screen.blit(moves_out.image, (440, 450))
    screen.blit(start_button.image, (148, 425))
    for i in range(3):
        screen.blit(record_lable[i].image, (450, 15 + i * 40))
    pygame.draw.rect(screen, button_color, (120, 420, 185, 70), 1)
    pygame.draw.rect(screen, (255, 255, 255), (125, 425, 175, 60), 1)
    if board.check_win():
        render_win()
    pygame.display.flip()

pygame.quit()
